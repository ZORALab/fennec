<!--
+++
title = "Contributors"
description = "Want to know Fennec's contributors? Here is the right place. We documented our contributors in this page."
keywords = ["fennec", "contributors", "list"]
authors = [ "ZORALab", "Holloway Chew Kean Ho" ]
date = "Mon Mar  4 14:07:25 +08 2019"
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Projects"
weight = 2
+++
-->

# Contributors
Here are the list of project contributors and sponsors to make Fennec project
available open-source and to all.

<br/>

## Sponsors

[![ZORALab](https://lh3.googleusercontent.com/jwB6L7Jl2A9DkqolBhnR915KvgMid66zieSDqh5hMK6oMJR81mw_QWzynMEJL-R_8yA1yKXawvGy3wb3pFmfeoCu-mVrBFPhY6BaORathrfVogDNUvZ5WvKtpH9s7faaOMDP9_KKUBkWny87gnX1mNDclbJn1NqY6rF237CbYHSyqjnYaFtACRdqR7Pl4ZZeqzdaybQWsF2KWbyqSNfQA-wsMFGf9An_xxrvS-oUKqB7G_cxotfFqpK2NvVdhQ80G7scO3WHNqlhTKmcPrMCk-BlTyNCmLsaqeAw9rfWV66ty_ximBVMnOOHVOwW4hb3RCGG9X130FiaX9Vj4Retu4wSW8DpvrvBGJqfMqqYqNo4w2c0oQUiDn3sH0IKyNMuTGiegXZUt-IcLK437uwf_FGO42XBorQSB4bEt0ZoQweUVmOADM03VEivIKFaM8bI0obdBtFoOCSz29mUeF1Pa5Oc7EJPfGakC5TAf5HJsnbjYRvuVDsXshHuuZ7JH1JXs93SE_8mf-3flN3N18ORwvTIKsiGJ4t38pEk9QWqXt2PToBuCJBjBdAWFUwWEU1_NOIM8mY_rpfOH_AmH_CtX0Ql6KUF-sIcdQzKKSn4cQjICOvPfF5bSHOzXWzEwHeqvjFmkRybHH5HORdk-BaGRZQQh3so3h9m=w300-h100-no)](https://www.zoralab.com)

> Want to be here? See [Sponsoring]({{< absLangLink "/projects/sponsoring/" >}}).

<br/>

## Contributors
### Maintainers
1. (Holloway) Chew, Kean Ho (`kean.ho.chew@zoralab.com`) - 2015 to present

### Developers
1. (Holloway) Chew, Kean Ho (`kean.ho.chew@zoralab.com`) - 2015 to present
