<!--
+++
title = "Sponsoring"
description = """
Want to contribute as a sponsor? This is the right place! This page shows the
necessary procedures for sponsoring Fennec!
"""
keywords = ["sponsor", "fennec", "contribute"]
authors = [ "ZORALab", "Holloway Chew Kean Ho" ]
date = "2018-08-08T10:36:12+08:00"
draft = true
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Projects"
weight = 4
+++
-->

# Sponsoring
Thank you for your interest in sponsoring Fennec. Currently Fennec is under
the following program for its development and availability maintenances:

1. Shepard Program from ZORALab Enterprise

You can help us by sponsoring us a one-time small tips (e.g. a cup of coffee)
through our selected payment channels. By sponsoring us, you help us to press
on development useful open-source softwares for the benefits of all!

<br/>

## What You'll Get
As a returning favor for those who help us via subscription sponsorship, we
will do:

1. A customized `300px x 100px` badge designed and placed in this project's
[Homepage]({{< absLangLink "/" >}}) and
[Contributors]({{< absLangLink "/projects/contributors" >}}) pages.
2. Mentioned in development and CHANGELOG!

<br/>

## Read Terms and Conditions
Be sure to read through the [Terms and Conditions](#1-terms-and-conditions) at
the attachment section below.

**Once you perform a payment transaction, you're automatically agreed to our
terms and conditions**.

<br/>

## Payment
Once you're ready, you can use any of our selected payment channels.

### One Time
This is the one-time only payment. It is useful for those who want to tip us
and move on.

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
	<input type="hidden" name="cmd" value="_s-xclick">
	<input type="hidden" name="hosted_button_id" value="J5ZSUB6XUZLNC">
	<input type="hidden" name="on0" value="Size">
	<input type="hidden" name="currency_code" value="USD">
	<input type="hidden" name="on2" value="Project">
	<input type="hidden" name="os2" value="Fennec" maxlength="200">

	<label for="software-sponsorship-one-time-option">Select Options</label>
	<select id="software-sponsorship-one-time-option" name="os0">
		<option value="8 bucks for coffee -">8 bucks for coffee - $8.00 USD</option>
		<option value="15 bucks for a bag -">15 bucks for a bag - $15.00 USD</option>
		<option value="20 bucks for awesomeness -">20 bucks for awesomeness - $20.00 USD</option>
	</select>

	<label for="software-sponsorship-one-time-full-name">Full Name</label>
	<input type="hidden" name="on1" value="Full Name">
	<input id="software-sponsorship-one-time-full-name" type="text" name="os1" maxlength="200">

	<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynow_LG.gif" border="0" name="submit" alt="Pay via PayPal">
</form>

> #### Any amount?
> Sure! We have Paypal.me terminal ready for you here:
>
> [![PayPal-Me](https://lh3.googleusercontent.com/8YAQpan47r2HKZn0BgdWQ2mfo0vlhrnQ0cKXbHrlt-R3PE3xeu4gO5KSVWWuFNILuMr9A5l9exMgNZ0EopBLXUocJh9F9fLZ4Q0USYR4QPax7RdAOnVl_K1fCIegPjB1thhfw2F43gd-d0gVPHURfKP1FZWKE_WMYLyyJV6UwCyE98CpjVqSzhRgCTCKTbilXfjJsrVVTLTCy8U9MA-m7NAYa67rvjZRkhVTPK7ncKZ1j4avNQxP4HdMR-F6sY3J-DQPsjNF3lXDxBxxLEdw0HrQQ2PLc4DhlHCa62N6dCs1zYNhoZPPGi8A36rPbynILwa1BK9CNeohlc1i5jVyIB6Fb1n-FiM_9L3yT3TYLcnQVH7M1LAqwsmDVgasE_wpG7-qDd5kVBaKAojkRcYbin93l2cl05M3VY0MKVkbE446D5cb_wJ-LJj6qL0az-Ut-u4cvsuT4Nnfl9E2GMt3mG2922B5kXJ-PadBIHB1YyGnhS8CAZJC5Te8TKZrdIld-tr2vOVhR0z2JB86dQUJ4vtsHeZAnazf_ad19gob-E5GJQPg2VDgwRfyvgMXDnMOKA-AJO4Tskohg9MPso4UN_vVsM3UwvVIc9mK1cB8BnrMBfm6Us-B6gT1XV4Teix1PgenhCNpZfFIK4vDtZKIuD8kSHE235w7=w200-h67-no)](https://www.paypal.me/zoralab/25)
>
> **NOTE**: Please be sure to edit the amount before you proceed to pay.
> You can click the price for editing.

<br/>

### Subscription
This is the subscription based payment. It is useful for those who want to
continuously supports us.

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
	<input type="hidden" name="cmd" value="_s-xclick">
	<input type="hidden" name="hosted_button_id" value="USCPNQVJKSJPA">
	<input type="hidden" name="on0" value="Select Plan">
	<input type="hidden" name="currency_code" value="USD">
	<input type="hidden" name="on2" value="Project">
	<input type="hidden" name="os2" value="Fennec">

	<label for="software-sponsorship-one-time-subscription">Select Options</label>
	<select id="software-sponsorship-one-time-subscription" name="os0">
		<option value="USD8">USD8 : $8.00 USD - monthly</option>
		<option value="USD15">USD15 : $15.00 USD - monthly</option>
		<option value="USD20">USD20 : $20.00 USD - monthly</option>
	</select>

	<label for="software-sponsorship-subscription-full-name">Full Name</label>
	<input type="hidden" name="on1" value="Full Name">
	<input id="software-sponsorship-subscription-full-name" type="text" name="os1" maxlength="200">
	<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_subscribe_LG.gif" border="0" name="submit" alt="Pay by PayPal">
</form>

> #### I want to Unsubscribe
> You can unsubscribe here: [![unsubscribe](https://www.paypalobjects.com/en_US/i/btn/btn_unsubscribe_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=6NQSE3QY32KPS)

<br/>

## Attachments
These are the attached documentations arranged in an unspecific order.

### 1. Terms And Conditions
We know you guys will probably just skip or skim through this section in
seconds, so we make it easier by summarizing it for you read through.

```
The following terms and conditions (these `Terms and Conditions`) have been
established by ZORALab Enterprise to set out the rights and obligations of a
sponsor of the above specified programme and become effective with
ZORALab Enterprise’s written confirmation of a sponsor application.

'We', 'our' or 'us' refers to ZORALab Enterprise.

'Project', 'project' refers to the software repository where this
Terms and Conditions document attached, deposited or linked within.

'Contributors' refers to the list of entity including us that contributes
our efforts into the project, such as code changes, issue creation and
management, Project management and releases.
```

<br/>

#### Change of Terms and Conditions
We will update the Terms and Conditions from time to time. Before applying
the new changes, we'll inform you via public announcement or emailing you.

```
We may sometimes make changes to these terms. If the material changes adversely
affect your rights under these terms, we will announce through official posting
on the website or sending you an email prior to the changes coming into effect.

Continuing to sponsor after the changes became effective means you accept the
new terms.
```

<br/>

#### Sponsorship Registration
By performing a payment to our sponsor account, you automatically agreed to
all the terms and conditions listed here.

```
By performing the payment via our published payment gateway, you are
automatically agreed and signed to all the terms and conditions listed in this
Terms and Conditions.
```

<br/>

#### Sponsor Materials
You keep the ownership of the content but give us the permission and right to
use it in the project you're sponsoring.

```
You keep full ownership over the materials created for the sponsorship but to
publish them in this repository's website and mentions, we need a license from
you.

By becoming the sponsor, you grant us a royalty-free, perpetual, irrevocable,
non-exclusive, sublicensable, worldwide license to use, reproduce, distribute,
perform, publicly display or prepare derivative works of your content.

The purpose of this license is to allow us to operate the Project, and to
promote your content on the Project. We and the Project may not use the content
posted by the creators in any way not authorized by the creator.
```

<br/>

#### Materials
Respect your materials with integrity by:

1. Not stealing from others, materials
2. Not containing nudity
3. Not legally offensive
4. Not promoting violent, criminal or hateful behaviour

This includes artwork as as well.

```
You may not post content that infringes on others' intellectual property or
proprietary rights, that displays genitals, focusing in on fully exposed
buttocks, or female breasts includind/excluding the nipple.
```

<br/>

#### Our Materials
You can use our contents from the Project within its permitted license; but
can't use it anywhere else without our written permission.

If unsure, ask us!

```
Contents we created in this Project is protected by copyright, licenses,
trademarks and trade secret laws. Some example are the text on the Project or
our website, logo, and the Project codebase.

You may not otherwise use, reproduce, distribute, perform, publicly display
or prepare derivative works of the Project contents outside of its permitted
licenses unless we give you permission in writing. Please ask if you have any
questions.
```

<br/>

#### Indemnity
If we got sued because of you, you have to help us to pay for it.

```
You will indemnify us from all losses and liabilities, including legal fees,
that arise from these terms or relate to your content. We reserve the right to
exclusive control over the defense of a claim covered by this clause. If we use
this right then you will help us in our defense.

Your obligation to indemnify under this clause also applies to our
affiliates, officers, directors, employees, agents,
third party service providers and the Project Contributors.
```

<br/>

#### Force Majeure and Termination
If there is a terminal distruption by the Force, we'll work together to find
other ways to keep continue/cancel the sponsorship.

```
In the event of fire, strike, civil commotion, act of terrorism, act of God, or
other force majeure making it impossible or impractical for us to operate the
sponsorship, we shall not be held in breach of its sponsorship obligations.

In such case, the sponsorship efforts shall be suspended by joint efforts from
you and us to find alternative ways of executing the sponsorship.

We may, at its sole discretion, terminate the sponsorship at any time by
returning pro rata any sponsorship fees paid by such sponsor for the remainder
of the Sponsorship year and you shall not be entitled to claim any damages.
```

<br/>

#### Laws
Any disputes with us must be resolved in Kuala Lumpur under Malaysia Law.

```
Malaysia law, excluding its conflict of law provisions, governs these terms for
the sponsorship policy. If a lawsuit does arise, both parties consent to the
exclusive jurisdiction and venue of the courts located in Kuala Lumpur,
Malaysia.
```

<br/>

#### Contact
If you have any questions, please email [legal@zoralab.com](email:legal@zoralab.com).
