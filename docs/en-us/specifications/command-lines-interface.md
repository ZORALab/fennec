<!--
+++
title = "Command Lines Interface"
description = "More details about command line interface related to Fennec? This page has it all."
keywords = ["fennec", "cli", "command line interface", "command line"]
authors = [ "ZoraLab", "Holloway Chew Kean Ho" ]
date = "Mon Mar  4 13:24:50 +08 2019"
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Specifications"
weight = 5
+++
-->

# Command Lines Interface
Fennec main program has a command lines interface (CLI) dediciated for
handling user repository setup. In any cases, depending on its script nature,
Fennec's CLI should comply to the "action" and then "options" format. Example:
```
3. -i, --install                install the fennec library inside this
                                repository. Its default location is in the
                                current directory. It optional value is given,
                                it will only install that component.
                                OPTIONAL VALUES:
                                1. -i fennec
                                  install the gitlab-ci fennec.

                                OPTIONAL ARGUMENTS:
                                1. -p, --path [/path/to/repo]
                                  the path you wish to install fennec library.
```
This way, it makes things clear with:

1. What is the `action` user wishes to do.
2. What are the available values and arguments for this `action`.

<br/>

## Help Display Format
Currently, Fennec complies to BaSHELL and UNIX system help printout format.
An overview would be:
```
Fennec
Your repository's one-stop shop for GitLab CI YAML templates.
-------------------------------------------------------------------------------
To use: ./fennec.sh [ACTION] [ARGUMENTS]

ACTIONS
1. -h, --help                   print help. Longest help is up
                                to this length for terminal
                                friendly printout.

2. -v, --version                print app version.

3. -i, --install                install the fennec library inside this
                                repository. Its default location is in the
                                current directory. It optional value is given,
                                it will only install that component.
                                OPTIONAL VALUES:
                                1. -i fennec
                                  install the gitlab-ci fennec.

                                OPTIONAL ARGUMENTS:
                                1. -p, --path [/path/to/repo]
                                  the path you wish to install fennec library.
...
```

1. The CLI has a title, specifying the `title` of the program.
2. The program's short description uses across various systems.
3. The split "actions - descriptions" descriptions for each actions.
4. The total width has a maximum of **80 characters** (it's terminal).

<br/>

### Split Actions - Descriptions Format
The "Split Actions - Descriptions" basically separate the `action` arguments
on the left while having their respective descriptions written to the right.

There are only 2 requirements:

1. The descriptions must be left-aligned.
2. There is enough space to show the separations.

A good format would be:
```
#. -sf, --shortFlag1          The descriptions for this action. Inside this
   -sf2, --shortFlag2         description, you describes the ncessary values
                              and available arguments for this action.
                              COMPULSORY/OPTIONAL VALUES:
                              1. -sf example/value
                                leave 2 spaces inside to describe the value.

                              COMPULSORY ARGUMENTS:
                              1. -sf3, --shortFlag3 <value>
                                leave 2 spaces to explain the flag. You can
                                also gives example. Here is one for --sf3.

                                Example:
                                1. ./program -sf example/value -sf3 /path/value

                              OPTIONAL ARGUMENTS:
                              1. -sf4, --shortFlag4 <value>
                                leave 2 spaces to explain the flag. Same as
                                above.
```
> #### Rationale
> 1. User don't need to scroll up and down figuring out what magic is needed to
>    perform an action.
> 2. Simplify the user experience. Keep it a simple action list.

<br/>

## Max Width and Height
The **width is strictly 80 characters**. It is a terminal based program so
please comply to its default.

The **height is limitless**. However, the output should be done in a way that
**it facilitates I/O redirection**, where user can dump the screen output into a
file for slow consumptions.

<br/>

## Reserved Actions
There are a list of reserved actions and arguments.

### Version
The reserved action and arguments are:
```bash
$ fennec -v
$ fennec --version
```
This is for Fennec to dump its current version number. It is reserved due to
its role in packaging and distributions.

<br/>

### Help
The reserved action and arguments are:
```bash
$ fennec -h
$ fennec --help
```
This is for Fennec to dump its available CLI actions and arguments. It is
reserved due to its only role for user to learn how to use Fennec.

