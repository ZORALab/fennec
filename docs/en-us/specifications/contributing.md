<!--
+++
title = "Contributing Guidelines"
description = "Want to contribute back to Fennec? This is definately the right place for guidelines."
keywords = ["Fennec", "contributing", "guidelines"]
authors = [ "ZORALab", "Holloway Chew Kean Ho" ]
date = "Mon Mar  4 18:06:50 +08 2019"
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Specifications"
weight = 8
+++
-->

# Contributing Guidelines
Thank you for reading this page. This page is suprisingly long but we will keep
a "too long; didn't read" section for those who are rushing.

<br/>

## Code of Conducts
We build community with respect to one another. Hence, please refrain from
discriminations and respect one another.

```
As contributors and maintainers of this project, we pledge to respect all
people who contribute through reporting issues, posting feature requests,
updating documentation, submitting pull requests or patches, and other
activities.

We are committed to making participation in this project a harassment-free
experience for everyone, regardless of level of experience, gender, gender
identity and expression, sexual orientation, disability, personal appearance,
body size, race, ethnicity, age, or religion.

Examples of unacceptable behavior by participants include the use of sexual
language or imagery, derogatory comments or personal attacks, trolling, public
or private harassment, insults, or other unprofessional conduct.

Project maintainers have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct. Project maintainers who do not
follow the Code of Conduct may be removed from the project team.

This code of conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community.

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by opening an issue or contacting one or more of the project
maintainers.

This Code of Conduct is adapted from the Contributor Covenant, version 1.1.0,
available at
https://www.contributor-covenant.org/version/1/1/0/code-of-conduct.html
```

<br/>

## Agreements
As you submit your codes for merging (upstreaming), you are automatically
agreed to our agreements depending on your origin of nature (individual or
entity).

1. [Individual](https://gitlab.com/ZORALab/Resources/blob/master/contributing/individual_agreement.md)
2. [Entity or company](https://gitlab.com/ZORALab/Resources/blob/master/contributing/corporate_agreement.md)

<br/>

## Raising Issues
If you have any questions, feel free to file an issue and label with as
`Discussion`. In case you know who is the maintainer or developer, you can
tag him into the issue as well.

Fennec facilitates and allows asking questions in the issue sections. Afterall,
that is what the `issue` section is about. Normally we'll review the values of
efforts, sharing and understanding the idea and by making the discussion open,
you can attract like-minded individuals to contribute together as well!

<br/>

## Upstreaming Codes
By upstreaming, we mean sending us codes for merge via any channels like email
patches, merge requests, and etc. To simplify the process, please make sure
you do the following:

1. Read through [**Specifications > Upstream Process**]({{< absLangLink "specifications/upstream-process" >}}).
2. Read through [**Specifications > Coding Style**]({{< absLangLink "specifications/coding-style" >}}).
3. Comply to all Fennec Design Specifications

They are available in various projects.

<br/>

## Sponsoring
Fennec is also open for monetary contribution as well. Currently, Fennec is
under ZORALab Enterprise's Shepherd program for its continuous supports over
time. To sponsor:

1. Read through [**Projects > Sponsorship**]({{< absLangLink "projects/sponsoring/" >}}).
2. Follow its instructions.
