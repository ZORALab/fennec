<!--
+++
title = "CI Pipelines"
description = "Fennec default CI Pipelines? This specification explains its automation process blocks. "
keywords = ["fennec", "pipelines", "ci", "gitlab"]
authors = [ "ZORALab", "Holloway Chew Kean Ho" ]
date = "Fri Mar  1 18:14:05 +08 2019"
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Specifications"
weight = 4
+++
-->

# CI Pipelines
Fennec introduces the 5 stages pipeline processes as its default CI process
blocks. These processes are specifically designed to cater testing services
at **commit** pushing traffic level, allowing developers to commit small and
commit fast easily.

Like any other CI automation, the range for these processes covers right from:

1. Developers committing into any branch.
2. Branch level regression testing.
3. Merge requesting testing.
4. Scheduled testing.
5. Packaging product.
6. Publishing documentations.
7. Upstream or Distribute product.

We will look through each processes in details alonside with branch management.

<br/>

## Preferred Branch Management
Fennec always set itself to maintain these branches in the repository:

1. `master` - production level quality with version release `tags`.
2. `staging` - **[OPTIONAL]** next production level releases to `master` branch.
3. `next` - the futuristics / edge development branch.

### Master Branch
`master` branch is **ONLY** branch holding the end product. This is where
your customers will be using and recognizing. It should be protected, freezed,
and tightly controlled (e.g. accepting merge only from `staging` or `next`
branches).

### Staging Branch
For large or with a lot of dependencies's projects, `staging` branch provides
a good way to cherry-pick commits from `next` branch and to create a
"point-controlled" update package. Then, the maintainers and developers can
thoroughly test this update package until it is stable to merge into the
`master` branch.

This branch is optional for small scale and well-coordinated repositories. The
only benefit for having this branch is to freeze a release point for thorough
testing (e.g. mission critical application) while leaving the development
release progress in tact in the `next` branch.


### Next Branch
`next` branch is the future, holding all the latest and greatest. This is the
branch every developers pushes his/her commits and releases into.

if the `staging` branch exists, `next` branch occasionally needs to be
rebased from the `master` branch upon each successful merge from `staging` to
`master`.

Otherwise, the release freezing point can be done in this branch, tested and
merge into `master`. The good side is that it simple compared to `staging`
branch but at the expense of interrupting development release progress.

<br/>

## Release Strategy
Fennec prefers **progressive point release strategy**.

This allows Fennec to easily freeze a particular progress, perform testing
onto it, package it, distribute or deploy, and publish the documents. This also
means Fennec is a strong supporter for
[Semantic Versioning](https://semver.org/).

### Semantic Versioning (Tagging)
This occurs on `master` branch for each successful merges. The repository
maintainer usually `tag` the latest commit with a version number, complying
to [semantic versioning](https://semver.org/) documentation.

Fennec supports the numbering pattern with the letter '`v`'. Here is an
example:
```
v[major].[minor].[patch]

v0.1.0
v0.2.0
v1.0.0
v1.1.25
```

By the default recommendations,

1. `major` number indicates **backward incompatible** changes, possibly breaking
things.
2. `minor` number indicates **backward compatible** changes, an update to the
previous version.
3. `patch` number indicates **small changes** like bug fixes, security
releases or documentation updates.

<br/>

## Stages
Fennec uses a 5 stages for its pipeline processes. We will review them in
details.

### Test
Test stage is the 1st stage among all. The goal is to run regression testing
like unit testing, etc.

It gets triggered when **any developers pushes a commit into any branch**.

Test stage works in **all branches**.

<br/>

### Package
Package stage is the 2nd stage among all. The goal is to build production
ready software and package them using the standard packager like `deb` packager
or `Snapcraft`.

It gets triggered when **test stage is passed, and only in selected braches**.

Package stage works in `staging` and `master` branches.

> `staging` branch is to test the packaging tools working properly.
>
> `master` branch is the actual packaging event.

<br/>

### Upstream
Upstream stage is the 3rd stage among all. The goal is to upstream or to
distribute the newly built package into the ecosystem.

It gets triggered when **package stage is passed, and only in selected
branches**.

Upstream stage works in `master` branch.

<br/>

### Publish
Publish stage is an independent stage, 4th stage among all. The goal is to
generate and publish the repository's documentations.

It gets triggered when **any developers pushes a commit into `next` branch**.

<br/>

### Tasklet
Tasklet stage is an independent stage, 5th stage among all. The goal is to
run independent tasks via the CI infrastructures, outside the 1st to 4th stages.

It gets triggered **based on its design, e.g. scheduler**.
