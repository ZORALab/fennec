<!--
+++
title = "Install Fennec"
description = "Want repository CI automation? Let's start with installation. To Install Fennec, it's very easy"
keywords = ["fennec", "install", "getting-started", "guide"]
authors = [ "ZORALab", "Holloway Chew Kean Ho" ]
date = "Tue Feb 26 11:52:56 +08 2019"
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Getting Started"
weight = 1
+++
-->

# Install Fennec
Fennec is available in via the some official repository depending on your
system and package manager. Currently, Fennec is primarily supported via:

1. Debian based Linux distribution.
2. Ubuntu Launchpad

<br/>

## Snapcraft (Recommended)
Fennec has great supports via Snapcraft.io distribution center. To install
via Snapcraft, simply use the following command:
```bash
$ sudo snap install fennec
```

Fennec's Snapcraft site is available at: **[https://snapcraft.io/fennec](https://snapcraft.io/fennec)**.


<br/>

## Ubuntu / Ubuntu Launchpad
Fennec is available via Ubuntu Launchpad. You need to perform a few steps
in order to install Fennec properly:
```bash
$ sudo add-apt-repository ppa:zoralab/fennec
$ sudo apt update -y
$ sudo apt install fennec -y
```

Fennec's Launchpad site is available at: **[https://launchpad.net/~zoralab/+archive/ubuntu/fennec](https://launchpad.net/~zoralab/+archive/ubuntu/fennec)**.

<br/>

## Redhat / Fedora
*planned*.

<br/>

## Windows
*planned*.

<br/>

## Mac
*planned*.
