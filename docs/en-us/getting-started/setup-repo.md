<!--
+++
title = "Setup Repo"
description = "After installation? Let's setup fennec the repo. This page guides you through how to establish a usable CI automation."
keywords = ["setup", "repo", "fennec"]
authors = [ "ZORALab", "Holloway Chew Kean Ho" ]
date = "Tue Feb 26 14:38:52 +08 2019"
draft = false
type = ""
layout = "single"
# thumbnailURL = "#"

[menu.main]
parent = "Getting Started"
weight = 2
+++
-->

# Setup Repo
Setting up your repo with Fennec is quite straight-forward. In this guide,
we also include how to update Fennec as well.

<br/>

## Setup Fennec into Repository
To setup Fennec into your repository, you simply just have to execute:
```bash
$ cd <repo_directory>
$ fennec --install
```
This will install a `.fennec` inside you `<repo_directory>`.

> ### IMPORTANT NOTE
>
> DO NOT edit anything inside the `.fennec directory`. They will be overridden
> without mercy upon the next update.

<br/>

## Update Fennec in Repository
Fennec is released using point strategy so from time to time, you might be
interested with upgrading .fennec directory. To do so, use:
```bash
$ fennec --update
```

> ### NOTE
>
> It is not **MANDATORY** to get the latest and greatest from fennec master
> branch.

> Remember, Fennec is a tool for your repository. If there is no issue at any
> given point, don't do any upgrade. Just like the old idiom says:
>
> ***if nothing breaks, leave it.***
