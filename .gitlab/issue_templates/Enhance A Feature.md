# Description
Describe WHAT is the feature you suggest to enhance here.
Describe WHAT are to be enhanced.



## The Story Behind
Tell us about the WHY and your inspiration.



## Current Behavior
Explain the current behavior.



## Expected Behavior
Explain the expected behavior.



## List out the MUST HAVE Personality
1. Describe the things that are MUST HAVE.
2. List them out like the way it is here.
3. Remember to leave a space after the numbering.



## Urgency
> Leave only ONE. Delete the rest including this label.

1. ***Code Black*** - Somebody's life is at stake (e.g. medical equipment, national security)
2. ***Code Red*** - Immediate Attention. (e.g. My business is not running at all)
3. ***Code Blue*** - Please plan out. (e.g. I can survive for now)
4. ***Code Green*** - Not urgent. (e.g. take your time)



----
----

## Automated Settings
> Don't worry! Let Cory takes over here.

/label ~"Discussion" ~"Enhancement"
