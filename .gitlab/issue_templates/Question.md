# Urgency
> Leave only ONE. Delete the rest including this label.
1. ***Code Black*** - Somebody's life is at stake (e.g. medical equipment, national security)
2. ***Code Red*** - Immediate Attention. (e.g. My business is not running at all)
3. ***Code Blue*** - Please plan out. (e.g. I can survive for now)
4. ***Code Green*** - Not urgent. (e.g. take your time)



---
---

```
IMPORTANT NOTE
If you choose **Red**, and **Black**, you should contact us directly
at discord channel instead of raising this issue here:

https://discordapp.com/invite/22Bw92v

Otherwise, please keep in mind a reply can take 1-7 hours to days.
```



# Description
Describe WHAT for your question. Leave your question in the Title.
Please make sure you leave an empty line before those 2 horizontal
lines below.





----
----

## Automated Settings
> Don't worry! Let Cory takes over here.

/label ~"Discussion" ~"Question"
