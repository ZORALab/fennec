#!/bin/bash
################################
# User Variables               #
################################
VERSION="1.4.0"

################################
# App Variables                #
################################
action=""
object_type=""
list_option="all"
repo_fennec=".fennec"
repo_path="${PWD}"
fennec_path="${PWD}/${repo_fennec}"
source_path=""
source_fennec="fennec"
default_source_path="/usr/share/${source_fennec}"


################################
# Functions                    #
################################
print_version() {
	echo $VERSION
}

_startup() {
	if [[ ! -d "$repo_path" ]]; then
		1>&2 echo "[ ERROR ] not a valid path: $repo_path"
		exit 1
	fi

	# Package checking
	if [[ "$SNAP" != "" ]]; then
		source_path="${SNAP}/${source_fennec}"
	elif [[ -d "$default_source_path" ]]; then
		source_path="$default_source_path"
	else
		1>&2 echo "[ ERROR ] unable to find source files."
		exit 1
	fi
}

_uninstall_fennec() {
	location="${repo_path}/${repo_fennec}"
	if [[ ! -d "$location" ]]; then
		return 0
	fi

	rm -rf "$location" > /dev/null
}

uninstall() {
	_startup

	case "$object_type" in
	fennec)
		_uninstall_fennec
		;;
	*)
		_uninstall_fennec
		;;
	esac
}

_install_fennec() {
	if [[ "$repo_path" == "" ]]; then
		1>&2 echo "[ ERROR ] empty repo path."
		exit 1
	fi

	if [[ -d "${repo_path}/${repo_fennec}" ]]; then
		1>&2 echo "[ ERROR ] fennec already exists in the repository."
		exit 1
	fi

	cp -r "${source_path}/${source_fennec}" "${repo_path}/${repo_fennec}"
}

install() {
	_startup

	case "$object_type" in
	fennec)
		_install_fennec
		;;
	*)
		_install_fennec
		;;
	esac
}

_update_fennec() {
	_uninstall_fennec
	_install_fennec
}

update() {
	_startup

	case "$object_type" in
	fennec)
		_update_fennec
		;;
	*)
		_update_fennec
		;;
	esac
}

list() {
	search_path="./.fennec"

	if [ ! -d "$search_path" ]; then
		1>&2 echo "[ ERROR ] are you at the root repository directory?"
		exit 1
	fi

	case "$list_option" in
	core)
		search_path="${search_path}/core"
		antigrep_args="base"
		;;
	base)
		search_path="${search_path}/core"
		grep_args="base"
		;;
	publish)
		search_path="${search_path}/publish"
		;;
	test)
		search_path="${search_path}/test"
		;;
	package)
		search_path="${search_path}/package"
		;;
	upstream)
		search_path="${search_path}/upstream"
		;;
	tasklet)
		search_path="${search_path}/tasklet"
		;;
	*)
		;;
	esac

	case "$os_family" in
	linux)
		os_family="linux"
		;;
	windows)
		os_family="windows"
		;;
	macos)
		os_family="macos"
		;;
	*)
		os_family=""
		;;
	esac

	if [ ! -z "$antigrep_args" ]; then
		find "$search_path" -type f -name "*.yml" 2> /dev/null \
			| grep "$grep_args" \
			| grep "$os_family" \
			| grep -v "$antigrep_args"
	else
		find "$search_path" -type f -name "*.yml" 2> /dev/null \
			| grep "$grep_args" \
			| grep "$os_family"
	fi
}

################################
# CLI Parameters and Help      #
################################
print_help() {
	echo "\
Fennec
Your repository's one-stop shop for GitLab CI YAML templates.
-------------------------------------------------------------------------------
To use: $0 [ACTION] [ARGUMENTS]

ACTIONS
1. -h, --help			print help. Longest help is up
				to this length for terminal
				friendly printout.


2. -v, --version		print app version.


3. -i, --install		install the fennec library inside this
				repository. Its default location is in the
				current directory. It optional value is given,
				it will only install that component.
				OPTIONAL VALUES:
				1. -i fennec
				  install the gitlab-ci fennec.

				OPTIONAL ARGUMENTS:
				1. -p, --path [/path/to/repo]
				  the path you wish to install fennec library.


4. -u, --uninstall		uninstall the fennec library inside this
				repository. Its default location is in the
				current directory. If optional value is given,
				it will only uninstall that component.
				OPTIONAL VALUES:
				1. -u fennec

				OPTIONAL ARGUMENTS:
				1. -p, --path [/path/to/repo]
				  the path you wish to install fennec library.


5. -U, --update			update the fennec library inside this
				repository. Its default location is in the
				current directory. If optional value is given,
				it will only update that component.
				OPTIONAL VALUES:
				1. -U fennec

				OPTIONAL ARGUMENTS:
				1. -p, --path [/path/to/repo]
				  the path you wish to update fennec library.


6. -l, --list			list available recipes for includes. Its default
				listing is to list all categories. Otherwise,
				it accepts the TYPE values for it to search.
				OPTIONAL VALUES:
				1. -l
				   -l all
				  List all the recipes in .fennec directory.

				2. -l core
				  List all core recipes.

				3. -l base
				  List all core base recipes.

				4. -l publish
				  List all publish recipes.

				5. -l test
				  List all test recipes.

				6. -l package
				  List all package recipes.

				7. -l upstream
				  List all upstream recipes.

				8. -l tasklet
				  List all tasklet recipes.


				OPTIONAL ARGUMENTS:
				1. -f, --os-family [TYPE]
				  Filter the listing with only os-family type.
				  Only accepts 'windows', 'linux', 'macos' as
				  value.
				  Example:
				  1) ./fennec -l core -f windows
				  2) ./fennec -l base -f linux
				  3) ./fennec -l -f macos
"
}

run_action() {
case "$action" in
"h")
	print_help
	;;
"i")
	install
	;;
"l")
	list
	;;
"u")
	uninstall
	;;
"U")
	update
	;;
"v")
	print_version
	;;
*)
	echo "[ ERROR ] - invalid command."
	return 1
	;;
esac
}

process_parameters() {
while [[ $# != 0 ]]; do
case "$1" in
-f|--os-family)
	if [[ "$2" != "" && "${2:0:1}" != "-" ]]; then
		os_family="$2"
		shift 1
	fi
	;;
-h|--help)
	action="h"
	;;
-i|--install)
	if [[ "$2" != "" && "${2:0:1}" != "-" ]]; then
		object_type="$2"
		shift 1
	fi
	action="i"
	;;
-l|--list)
	if [[ "$2" != "" && "${2:0:1}" != "-" ]]; then
		list_option="$2"
		shift 1
	fi
	action="l"
	;;
-p|--path)
	if [[ "$2" != "" && "${2:0:1}" != "-" ]]; then
		repo_path="$2"
		shift 1
	fi
	;;
-u|--uninstall)
	action="u"
	;;
-U|--update)
	action="U"
	;;
-v|--version)
	action="v"
	;;
*)
	;;
esac
shift 1
done
}

main() {
	process_parameters $@
	run_action
	if [[ $? != 0 ]]; then
		exit 1
	fi
}

if [[ $BASHELL_TEST_ENVIRONMENT != true ]]; then
	main $@
fi
