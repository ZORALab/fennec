#!/bin/bash
TITLE='update_pages_domain is able to call the API correctly'
SUITE='fennec/api/gitlab/pages_domains/update_pages_domain'
DESCRIPTION='
The update_pages_domain is able to call GitLab API properly with and without
SSL certificates and keys.
'
tprint_test_description "$TITLE" "$SUITE" "$DESCRIPTION"
################################################################################
tprint "prepare fennec into test directory"
cp -r "$CURRENT_DIRECTORY/.fennec" "$TEST_TEMP_DIRECTORY/."
HOME="$TEST_TEMP_DIRECTORY/username"
mkdir -p "$HOME"
cd "$TEST_TEMP_DIRECTORY"

GITLAB_API_URL="https://gitlab.com/api/v4"
GITLAB_PRIVATE_TOKEN="abcdef123--abcdef123"
gitlab_id="groupname%2Fprojectname"
domain_value="www.example.com"
ssl_pem_path_value="$TEST_TEMP_DIRECTORY/ssl.pem"
ssl_key_path_value="$TEST_TEMP_DIRECTORY/ssl.key"
cp "$TEST_SCRIPT_DIRECTORY/fennec/api/gitlab/pages_domains/dummy_ssl.cert" \
	"$ssl_pem_path_value"
cp "$TEST_SCRIPT_DIRECTORY/fennec/api/gitlab/pages_domains/dummy_ssl_key" \
	"$ssl_key_path_value"

curl_basic_check() {
	Turl="${GITLAB_API_URL}/projects/${gitlab_id}/pages/domains/${domain}"

	if [[ "$@" != *"--header PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN"* ]]; then
		verdict --failed "did not generate private token header."
		exit $TEST_FAIL
	fi

	if [[ "$@" != *"--form domain=$domain"* ]]; then
		verdict --failed "did not create form data for domain."
		exit $TEST_FAIL
	fi

	if [[ "$@" != *"--request PUT $Turl"* ]]; then
		verdict --failed "did not request the correct url."
		exit $TEST_FAIL
	fi
}

curl_ssl_check() {
	if [[ "$@" != *"--form certificate=@$ssl_pem_path_value"* ]]; then
		verdict --failed "did not include ssl certificate."
		exit $TEST_FAIL
	fi

	if [[ "$@" != *"--form key=@$ssl_key_path_value"* ]]; then
		verdict --failed "did not include ssl key."
		exit $TEST_FAIL
	fi
}

curl_cert_or_key_only_check() {
	if [[ "$@" == *"--form certificate=@"* ]]; then
		verdict --failed "still include certificate form."
		exit $TEST_FAIL
	fi

	if [[ "$@" == *"--form key=@"* ]]; then
		verdict --failed "still include key form."
		exit $TEST_FAIL
	fi
}

tprint "testing update_pages_domain without ssl certificate and key"
source "$CURRENT_DIRECTORY/.fennec/api/gitlab/pages_domains.sh"
domain="$domain_value"
curl() {
	curl_basic_check $@
}
ret=$(update_pages_domain)
if [[ "$ret" != "" ]]; then
	echo "$ret"
	exit $TEST_FAIL
fi

tprint "testing update_pages_domain with ssl certificate and key"
source "$CURRENT_DIRECTORY/.fennec/api/gitlab/pages_domains.sh"
domain="$domain_value"
ssl_pem_path="$ssl_pem_path_value"
ssl_key_path="$ssl_key_path_value"
curl() {
	curl_basic_check $@
	curl_ssl_check $@
}
ret=$(update_pages_domain)
if [[ "$ret" != "" ]]; then
	echo "$ret"
	exit $TEST_FAIL
fi

tprint "testing update_pages_domain with ssl certificate only"
source "$CURRENT_DIRECTORY/.fennec/api/gitlab/pages_domains.sh"
domain="$domain_value"
ssl_pem_path="$ssl_pem_path_value"
unset ssl_key_path
curl() {
	curl_basic_check $@
	curl_cert_or_key_only_check $@
}
ret=$(update_pages_domain)
if [[ "$ret" != "" ]]; then
	echo "$ret"
	exit $TEST_FAIL
fi

tprint "testing update_pages_domain with ssl key only"
source "$CURRENT_DIRECTORY/.fennec/api/gitlab/pages_domains.sh"
domain="$domain_value"
ssl_key_path="$ssl_key_path_value"
unset ssl_pem_path
curl() {
	curl_basic_check $@
	curl_cert_or_key_only_check $@
}
ret=$(update_pages_domain)
if [[ "$ret" != "" ]]; then
	echo "$ret"
	exit $TEST_FAIL
fi

verdict --passed
exit $TEST_PASS
